from codecs import encode as dc
from json import dump as dm, load as ld
from jaraco.clipboard import copy as cp

def init():
    global fl, psswds_jsn, dt, ttls, ncd
    fl = 'psswds.json'

    # Read or create.
    try: psswds_jsn = open(fl, encoding='utf-8')
    except: psswds_jsn = open(fl, 'w', encoding='utf-8')

    # Read or fill.
    try: dt = ld(psswds_jsn)
    except: dt = {}

    psswds_jsn.close()
    ttls = [e for e in dt]
    ncd = '31_tor'[::-1]

def mn():
    init()
    print('S3l3ct 4n 0pt10n: ')
    for i, e in enumerate(ttls):
        print(f'{i+1}) {dc(e, ncd)}')
    print(f'0) 4dd cr3d3nt141s.')
    sl(input('\n...: '))

def sl(s):
    if not s.isdecimal():
        return mn()
    elif int(s) == 0:
        return nw()
    elif int(s)-1 >= len(ttls):
        input('\n0UT 0F R4NG3!\n')
        return mn()
    c = '\nC4FFJ0EQ P0CV3Q G0 PY1CO04EQ!\n'
    print()
    for a, b in dt[ttls[int(s)-1]].items():
        print(f'{dc(a, ncd)}: {dc(b, ncd)}')
    try:
        cp(dc(b, ncd))
        print(dc(c, ncd))
    except:
        pass
    input(dc('CE3FF 4AL X3L G0 3K1G!', ncd))

def nw():
    ttl = dc(input('T1TL3?\n...: '), ncd)
    usr = dc(input('USR?\n...: '), ncd)
    psswd = dc(input('PSSWD?\n...: '), ncd)
    dt[ttl] = dict([(dc('usr', ncd), usr), (dc('psswd', ncd), psswd)])
    print(dt)
    psswds_jsn = open(fl, 'w', encoding='utf-8')
    dm(dt, psswds_jsn, indent=2)
    psswds_jsn.close()
    print('4DD3D!')
    return mn()

print('W31c0m3 t0 PssWds-Gttr\n')
mn()
